{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

type family Id a where
  Id a = a

main :: IO ()
main = B (Just 5) `seq` (B 5 :: TrueB Id) `seq` G (Just 5) `seq` (G 5 :: TrueG Id) `seq` pure ()

-- TcM
-- In data type:False:True
-- Nothing
data B (a :: Type -> Type) = B (a Int)

-- TcM&
-- In newtype:False:True
-- Nothing
newtype G (a :: Type -> Type) = G (a Int)

-- We specify the expected matchability here. If the defaulting scheme is
-- working as intended, it should compile.
type TrueB :: forall m. (Type -> @m Type) -> @'Matchable Type
type TrueB = B

type TrueG :: forall m. (Type -> @m Type) -> @'Matchable Type
type TrueG = G
