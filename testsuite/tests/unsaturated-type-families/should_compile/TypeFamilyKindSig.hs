{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

type family Id a where
  Id a = a

main :: IO ()
main = (Just 5 :: TrueL Maybe) `seq` (5 :: TrueL Id) `seq` pure ()

-- TcM
-- In type family:False:True
-- Nothing
type family L (a :: Type -> Type)
type instance L a = a Int

-- We specify the expected matchability here. If the defaulting scheme is
-- working as intended, it should compile.
type TrueL :: forall m. (Type -> @m Type) -> @'Unmatchable Type
type TrueL = L
