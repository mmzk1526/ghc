{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = I succ `seq` I' pred `seq` J succ `seq` J' pred `seq` pure ()

-- TcM
-- InDataDecl:False:False
-- Just 'Unmatchable
data I = I (Int -> Int)

-- TcM
-- InDataDecl:True:False
-- Just 'Unmatchable
data I' = I' ((->) Int Int)

-- TcM
-- InDataDecl:False:False
-- Just 'Unmatchable
newtype J = J (Int -> Int)

-- TcM
-- InDataDecl:True:False
-- Just 'Unmatchable
newtype J' = J' ((->) Int Int)
