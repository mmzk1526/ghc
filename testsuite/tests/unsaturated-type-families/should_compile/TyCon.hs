{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = Id 5 `seq` pure ()

data Id a = Id a

-- We specify the expected matchability here. If the defaulting scheme is
-- working as intended, it should compile.
type TrueId :: Type -> @'Matchable Type
type TrueId = Id
