{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (5 :: TrueC Id) `seq` (Just 7 :: TrueC Maybe) `seq` pure ()

type family Id a where
  Id a = a

-- TcM
-- InTypeSyn:True:True
-- Nothing
type C a = a Int

-- We specify the expected matchability here. If the defaulting scheme is
-- working as intended, it should compile.
type TrueC :: forall m. (Type -> @m Type) -> @'Unmatchable Type
type TrueC = C
