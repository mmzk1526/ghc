{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = a succ `seq` a' pred `seq` pure ()

-- TcM
-- InFunSig:True:False
-- Just 'Unmatchable
-- TcM
-- InFunSig:True:False
-- Just 'Unmatchable
-- TcM
-- InFunSig:True:False
-- Just 'Unmatchable
a :: (Int -> Int) -> (Int -> Int)
a = id

-- TcM
-- InFunSig:False:False
-- Just 'Unmatchable
-- TcM
-- InFunSig:False:False
-- Just 'Unmatchable
-- TcM
-- InFunSig:False:False
-- Just 'Unmatchable
a' :: (->) ((->) Int Int) ((->) Int Int)
a' = a
