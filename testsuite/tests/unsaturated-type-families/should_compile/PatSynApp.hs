{-# LANGUAGE UnsaturatedTypeFamilies, PatternSynonyms, ViewPatterns #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = case [] of
  A -> pure ()

-- TcM
-- InPatSyn:True:True
-- Just 'Matchable
pattern A :: f a
pattern A <- (const 1 -> 1)
