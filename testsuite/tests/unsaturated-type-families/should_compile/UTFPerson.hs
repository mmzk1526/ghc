{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RecordWildCards #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base hiding (M)
import Data.Proxy

main :: IO ()
main = pure ()

type family Id a where
  Id a = a

type Person :: forall m. (Type -> @m Type) -> Type
data Person f = Person { name :: f String, age :: f Int }

buildPerson :: Person Maybe -> Person Id
buildPerson Person {..} = Person <$> name <*> age
