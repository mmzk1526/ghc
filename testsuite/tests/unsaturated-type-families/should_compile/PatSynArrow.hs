{-# LANGUAGE UnsaturatedTypeFamilies, PatternSynonyms, ViewPatterns #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = case 5 of
  B 5 -> pure ()

-- TcM
-- InPatSyn:False:False
-- Just 'Unmatchable
pattern B :: Int -> Int
pattern B a <- (id -> a)
