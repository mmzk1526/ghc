{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base hiding (M)
import Data.Proxy

main :: IO ()
main = pure foo

type family Id a where
  Id a = a

type family Map (f :: a -> b) (x :: [a]) :: [b] where
  Map f '[] = '[]
  Map f (x ': xs) = f x ': Map f xs

foo :: (Map Id '[1,2,3] ~ '[1,2,3]) => ()
foo = ()
