{-# LANGUAGE UnsaturatedTypeFamilies, AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (d @[]) `seq` (d @Id) `seq` pure ()

type family Id a where
  Id a = a

-- TcM
-- InFunSig:False:True
-- Nothing
d :: forall a. Proxy (a :: Type -> Type)
d = Proxy
