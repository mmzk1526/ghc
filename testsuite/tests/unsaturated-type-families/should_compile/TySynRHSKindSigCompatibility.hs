{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (Proxy :: B Maybe) `seq` pure ()

-- TcM
-- InTypeSyn:False:True
-- Just 'Matchable
type B a = Proxy (a :: Type -> Type)
