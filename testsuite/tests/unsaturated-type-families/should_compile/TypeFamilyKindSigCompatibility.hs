{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeFamilies #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (Just 5 :: L Maybe) `seq` pure ()

-- TcM
-- In type family:False:True
-- Just 'Matchable
type family L (a :: Type -> Type)
type instance L a = a Int
