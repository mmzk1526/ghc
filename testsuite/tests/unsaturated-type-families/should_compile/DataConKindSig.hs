{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

type family Id a where
  Id a = a

main :: IO ()
main = (D (Proxy @Maybe)) `seq` (D' (Proxy @Maybe)) `seq` pure ()

-- TcM
-- InDataDecl:False:True
-- Just 'Matchable
data D a = D (Proxy (a :: Type -> Type))

-- TcM
-- InDataDecl:True:True
-- Just 'Matchable
data D' a = D' (Proxy (a :: (->) Type Type))
