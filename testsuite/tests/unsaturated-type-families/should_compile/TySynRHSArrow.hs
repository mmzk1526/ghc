{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (succ :: D Int) `seq` pure ()

-- TcM
-- InTypeSyn:True:False
-- Just 'Unmatchable
type D = (->) Int
