{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (R (Just 5) :: R) `seq` pure ()

-- TcM
-- InDataDecl:True:True
-- Just 'Matchable
data family R
data instance R = forall a. R (a Int)
