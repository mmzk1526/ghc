{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base hiding (O)
import Data.Proxy

main :: IO ()
main = pure ()

-- TcM
-- InTyFamPattern:True:True
-- Just 'Matchable
type family K a
type instance K (a b) = Int
