-- {-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (c @[] @Int) `seq` pure ()

-- TcM
-- InFunSig:False:True
-- Just 'Matchable
c :: forall (f :: Type -> Type) x. Proxy (f x)
c = Proxy
