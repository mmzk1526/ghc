{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (5 :: TrueId Int) `seq` (5 :: TrueId' Int) `seq` pure ()

type family Id a where
  Id a = a

type family Id' a
type instance Id' a = a

-- We specify the expected matchability here. If the defaulting scheme is
-- working as intended, it should compile.
type TrueId :: Type -> @'Unmatchable Type
type TrueId = Id

type TrueId' :: Type -> @'Unmatchable Type
type TrueId' = Id'
