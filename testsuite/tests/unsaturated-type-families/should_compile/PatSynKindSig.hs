{-# LANGUAGE UnsaturatedTypeFamilies, PatternSynonyms, ViewPatterns #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = case Proxy of
  D -> pure ()

-- TcM
-- InPatSyn:False:True
-- Just 'Matchable
pattern D :: Proxy (f :: Int -> Int)
pattern D = Proxy
