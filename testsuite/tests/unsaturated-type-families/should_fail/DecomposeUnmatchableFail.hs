{-# LANGUAGE UnsaturatedTypeFamilies, AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = pure ()

foo :: forall (f :: Type -> @'Unmatchable Type) g a b. (f a ~ g b) => a -> b
foo = id
