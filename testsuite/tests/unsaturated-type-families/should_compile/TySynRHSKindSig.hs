{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (Proxy :: TrueB Id) `seq` (Proxy :: TrueB Maybe) `seq` pure ()

type family Id a where
  Id a = a

-- TcM
-- InTypeSyn:False:True
-- Nothing
type B a = Proxy (a :: Type -> Type)

-- We specify the expected matchability here. If the defaulting scheme is
-- working as intended, it should compile.
type TrueB :: forall m. (Type -> @m Type) -> @'Unmatchable Type
type TrueB = B
