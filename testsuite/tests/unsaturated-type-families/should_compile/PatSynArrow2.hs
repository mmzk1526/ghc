{-# LANGUAGE UnsaturatedTypeFamilies, PatternSynonyms, ViewPatterns #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = case Proxy of
  C -> pure ()

-- TcM
-- InPatSyn:False:False
-- Just 'Unmatchable
pattern C :: Proxy (Int -> Int)
pattern C = Proxy
