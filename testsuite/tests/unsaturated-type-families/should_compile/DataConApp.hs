{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = A (Just 5) `seq` F (Just 7) `seq` pure ()

-- TcM
-- InDataDecl:True:True
-- Just 'Matchable
data A f = A (f Int)

-- TcM
-- InDataDecl:True:True
-- Just 'Matchable
newtype F a = F (a Int)

-- We specify the expected matchability here. If the defaulting scheme is
-- working as intended, it should compile.
type TrueA :: (Type -> @'Matchable Type) -> @'Matchable Type
type TrueA = A
