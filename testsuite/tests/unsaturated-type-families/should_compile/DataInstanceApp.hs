{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base hiding (O)
import Data.Proxy

main :: IO ()
main = (K :: K (Maybe Bool)) `seq` (O 5 :: O (Maybe Bool)) `seq` pure ()

-- TcM
-- InTyFamPattern:True:True
-- Just 'Matchable
data family K a
data instance K (a b) = K

-- TcM
-- InTyFamPattern:True:True
-- Just 'Matchable
data family O a
newtype instance O (a b) = O Int
