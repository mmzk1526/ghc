{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base hiding (M)
import Data.Proxy

main :: IO ()
main = M Just `seq` M' Just `seq` Q succ `seq` Q' pred `seq` pure ()

-- TcM
-- InDataDecl:False:False
-- Just 'Unmatchable
data family M
data instance M = M (Int -> Maybe Int)

-- TcM
-- InDataDecl:True:False
-- Just 'Unmatchable
data family M'
data instance M' = M' ((->) Int (Maybe Int))

-- TcM
-- InDataDecl:False:False
-- Just 'Unmatchable
data family Q
newtype instance Q = Q (Int -> Int)

-- TcM
-- InDataDecl:True:False
-- Just 'Unmatchable
data family Q'
newtype instance Q' = Q' ((->) Int Int)
