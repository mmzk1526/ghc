{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (N (Proxy @Maybe):: N) `seq` pure ()

-- TcM
-- InDataDecl:False:True
-- Just 'Matchable
data family N
data instance N = forall a. N (Proxy (a :: Type -> Type))
