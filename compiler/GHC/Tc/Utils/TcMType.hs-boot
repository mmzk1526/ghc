module GHC.Tc.Utils.TcMType where

import GHC.Tc.Types
import GHC.Types.Name
import GHC.Core.TyCo.Rep
import GHC.Tc.Types.Evidence
import Data.Maybe (Maybe)
import GHC.Core.TyCon (TyConFlavour)
import GHC.Tc.Types.Origin (TvCtxt)
import Data.Bool (Bool)

tcCheckUsage :: Name -> Mult -> TcM a -> TcM (a, HsWrapper)
maybeDefaultMatchability :: TvCtxt -> Bool -> Maybe Matchability
