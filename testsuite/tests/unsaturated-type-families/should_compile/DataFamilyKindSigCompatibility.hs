-- {-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeFamilies #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = L (Just 5) `seq` P (Just 7) `seq` pure ()

-- TcM
-- In data family:False:True
-- Just 'Matchable
data family L (a :: Type -> Type)
data instance L a = L (a Int)

-- TcM
-- In data family:False:True
-- Just 'Matchable
data family P (a :: Type -> Type)
newtype instance P a = P (a Int)
