{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (C :: C (Maybe Int)) `seq` pure ()

-- TcM
-- InDataDecl:True:True
-- Just 'Matchable
data C a where
  C :: C (a Int)
