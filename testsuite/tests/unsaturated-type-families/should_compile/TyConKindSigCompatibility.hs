-- {-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = B (Just 5) `seq` G (Just 5) `seq` pure ()

barB (b :: Maybe Int) = B b

-- TcM
-- In data type:False:True
-- Just 'Matchable
data B (a :: Type -> Type) = B (a Int)

-- TcM
-- In newtype:False:True
-- Just 'Matchable
newtype G (a :: Type -> Type) = G (a Int)
