{-# LANGUAGE UnsaturatedTypeFamilies, AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (c @[] @Int) `seq` (c @Id @Int) `seq` pure ()

type family Id a where
  Id a = a

-- TcM
-- InFunSig:False:True
-- Nothing
c :: forall (f :: Type -> Type) x. Proxy (f x)
c = Proxy
