-- {-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (Nothing :: A Maybe) `seq` (Nothing :: A' Maybe) `seq` pure ()

-- TcM
-- In type synonym:False:True
-- 'Matchable
type A (a :: Type -> Type) = a Int

-- TcM
-- In type synonym:True:True
-- 'Matchable
type A' (a :: (->) Type Type) = a Int
