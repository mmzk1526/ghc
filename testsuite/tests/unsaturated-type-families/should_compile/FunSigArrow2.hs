{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = e @Int `seq` pure ()

-- TcM
-- InFunSig:False:False
-- Just 'Unmatchable
e :: Proxy (a -> a)
e = Proxy
