{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

type family Id a where
  Id a = a

main :: IO ()
main = L (Just 5) `seq` (L 5 :: TrueL Id) `seq` P (Just 5) `seq` (P 5 :: TrueP Id) `seq` pure ()

-- TcM
-- In data family:False:True
-- Nothing
-- TcM
-- InTyFamPattern:True:False
-- Nothing
data family L (a :: Type -> Type)
data instance L a = L (a Int)

-- TcM
-- In data family:False:True
-- Nothing
-- TcM
-- InTyFamPattern:True:False
-- Nothing
data family P (a :: Type -> Type)
newtype instance P a = P (a Int)

-- We specify the expected matchability here. If the defaulting scheme is
-- working as intended, it should compile.
type TrueL :: forall m. (Type -> @m Type) -> @'Matchable Type
type TrueL = L

type TrueP :: forall m. (Type -> @m Type) -> @'Matchable Type
type TrueP = P
