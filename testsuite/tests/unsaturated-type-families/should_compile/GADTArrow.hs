{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

type family Id a where
  Id a = a

main :: IO ()
main = E `seq` pure ()

-- TcM
-- InDataDecl:True:False
-- Just 'Unmatchable
data E a where
  E :: E ((->) b)
