{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base hiding (M)
import Data.Proxy

main :: IO ()
main = (Just :: M) `seq` (Just :: M') `seq` pure ()

-- TcM
-- InDataDecl:False:False
-- Just 'Unmatchable
type family M
type instance M = (Int -> Maybe Int)

-- TcM
-- InDataDecl:True:False
-- Just 'Unmatchable
type family M'
type instance M' = ((->) Int (Maybe Int))
