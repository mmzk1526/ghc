{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = pure ()

type Id :: Type -> @'Matchable Type
type family Id a where
  Id a = a

type Id' :: Type -> @'Matchable Type
type family Id' a
type instance Id' a = a
