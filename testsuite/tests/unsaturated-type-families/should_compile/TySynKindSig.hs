{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy
import GHC.TypeError

import GHC.Exts

type Key a = Any


main :: IO ()
main = (5 :: A Id) `seq` (Nothing :: A Maybe) `seq` (5 :: A' Id) `seq` (Nothing :: A' Maybe) `seq` pure ()

type family Id a where
  Id a = a

-- TcM
-- In type synonym:False:True
-- Nothing
type A :: forall m. (Type -> @m Type) -> @'Unmatchable Type
type A (a :: Type -> Type) = a Int

-- TcM
-- In type synonym:True:True
-- Nothing
type A' (a :: (->) Type Type) = a Int

-- We specify the expected matchability here. If the defaulting scheme is
-- working as intended, it should compile.
type TrueA :: forall m. (Type -> @m Type) -> @'Unmatchable Type
type TrueA = A

type TrueA' :: forall m. (Type -> @m Type) -> @'Unmatchable Type
type TrueA' = A'
