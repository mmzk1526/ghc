{-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (b @[] @Int) `seq` pure ()

-- TcM
-- InFunSig:True:True
-- Just 'Matchable
b :: Proxy (f x)
b = Proxy
