{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (Just 7 :: C Maybe) `seq` pure ()

-- TcM
-- InTypeSyn:True:True
-- Just 'Matchable
type C a = a Int
