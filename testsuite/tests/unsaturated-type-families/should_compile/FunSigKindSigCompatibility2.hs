-- {-# LANGUAGE UnsaturatedTypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

import Data.Kind (Type)
import GHC.TypeNats
import GHC.Base
import Data.Proxy

main :: IO ()
main = (d @[]) `seq` pure ()

-- TcM
-- InFunSig:False:True
-- Just 'Matchable
d :: forall a. Proxy (a :: Type -> Type)
d = Proxy
